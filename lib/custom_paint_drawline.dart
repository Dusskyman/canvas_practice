import 'package:flutter/material.dart';

class CustomPainterDrawLine extends CustomPainter {
  final double sliderValue;
  final double procent;

  final Color color;
  final Paint fillPainter;
  final Paint linePainter;

  CustomPainterDrawLine({this.procent, this.sliderValue, this.color})
      : fillPainter = Paint()
          ..color = color
          ..style = PaintingStyle.fill,
        linePainter = Paint()
          ..color = color
          ..style = PaintingStyle.stroke
          ..strokeWidth = 2.5;

  @override
  void paint(Canvas canvas, Size size) {
    // _paintCircle(canvas, size);
    _paintLine(canvas, size);
    _paintBlock(canvas, size);
  }

  _paintCircle(Canvas canvas, Size size) {
    canvas.drawCircle(Offset(0.0, size.height), 5.0, fillPainter);
    canvas.drawCircle(Offset(size.width, size.height), 5.0, fillPainter);
  }

  _paintLine(Canvas canvas, Size size) {
    Path path = Path();
    path.moveTo(0.0, size.height * 0.5);
    path.lineTo(size.width, size.height * 0.5);
    canvas.drawPath(path, linePainter);
  }

  _paintBlock(Canvas canvas, Size size) {
    // Rect sliderRect = Offset(sliderValue, size.height - 5) & Size(5.0, 10);
    // canvas.drawRect(sliderRect, fillPainter);
    canvas.drawCircle(Offset(sliderValue, size.height * 0.5), 5.0, fillPainter);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
