import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:general_flutter_practice_7/custom_paint_drawline.dart';

class LineSlider extends StatefulWidget {
  final double height;
  final double width;

  final double minvalue;
  final double value;
  final double maxvalue;
  final Function(double) onChanged;

  const LineSlider({
    this.height = 50.0,
    this.width = 350,
    this.minvalue,
    this.maxvalue,
    this.value,
    this.onChanged,
  });

  @override
  _LineSliderState createState() => _LineSliderState();
}

class _LineSliderState extends State<LineSlider> {
  double drapPos = 0;
  double procent = 0;

  void _updateDragPosition(Offset val) {
    double newDragPos = 0;
    if (val.dx <= 0) {
      newDragPos = 0;
    } else if (val.dx >= widget.width) {
      newDragPos = widget.width;
    } else {
      newDragPos = val.dx;
    }
    setState(() {
      drapPos = newDragPos;
      procent =
          ((drapPos / widget.width) * (widget.maxvalue - widget.minvalue)) +
              widget.minvalue;
      widget.onChanged(procent);
    });
  }

  void _onDragUpdate(BuildContext context, DragUpdateDetails update) {
    RenderBox box = context.findRenderObject();
    Offset offset = box.globalToLocal(update.globalPosition);
    _updateDragPosition(offset);
    // print(offset.dx);
  }

  bool _checkValues() {
    if (widget.maxvalue <= widget.minvalue) {
      log('minvalue < maxvalue is not true');
      return false;
    }
    if (widget.value < widget.minvalue) {
      log('value => minvalue is not true');
      return false;
    }
    if (widget.value > widget.maxvalue) {
      log('value <= maxvalue is not true');
      return false;
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      drapPos = ((widget.value - widget.minvalue) /
              (widget.maxvalue - widget.minvalue)) *
          widget.width;
      print(drapPos);
    });
  }

  @override
  Widget build(BuildContext context) {
    return _checkValues()
        ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Text('${procent.toStringAsFixed(0)}'),
              GestureDetector(
                child: Container(
                    // color: Colors.green,
                    width: widget.width,
                    height: 50,
                    child: CustomPaint(
                      painter: CustomPainterDrawLine(
                        color: Colors.red,
                        sliderValue: drapPos,
                        procent: procent,
                      ),
                    )),
                onHorizontalDragUpdate: (DragUpdateDetails update) =>
                    _onDragUpdate(context, update),
              ),
            ],
          )
        : null;
  }
}
